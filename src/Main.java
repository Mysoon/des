import arrays.DesCipher;
import arrays.DesConstances;
import arrays.DesDecipher;
import arrays.SharedOperations;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        System.out.println("ORIGINAL MESSAGE:");
        for(int i=1;i<=64;i++){

            System.out.print(DesConstances.MSG.get(i-1));
            if(i%8==0)System.out.println();
        }

        System.out.println("CIPHERED TEXT: ");
        LinkedList<Integer> cipheredMsg =DesCipher.cipher(DesConstances.MSG,DesConstances.KEY);

        for(int i=1;i<=64;i++){

            System.out.print(cipheredMsg.get(i-1));
            if(i%8==0)System.out.println();
        }

        System.out.println();


        LinkedList<Integer> decipherResult =DesDecipher.decipher(new ArrayList<>(cipheredMsg),DesConstances.KEY);
        System.out.println("DECIPHERED TEXT: ");
        for(int i=1;i<=64;i++){

            System.out.print(decipherResult.get(i-1));
            if(i%8==0)System.out.println();
        }

       /* LinkedList<Integer> message = new LinkedList<>(List.of(1,0,0,1,1,1));
        System.out.println("Value of S Block Main");
        LinkedList<Integer> ofSBlock = SharedOperations.calculateOfSBlock(message, DesConstances.LIST_OF_S_BLOCKS.get(7));
        ofSBlock.forEach(System.out::print);
        System.out.println();
*/
    }

}
