package arrays;

import javax.crypto.spec.DESedeKeySpec;
import javax.swing.*;
import java.sql.SQLOutput;
import java.util.*;
import java.util.stream.Stream;

import static arrays.DesConstances.LIST_OF_S_BLOCKS;
//TODO: Zle w wyliczaniu S blocku
public class SharedOperations {
    private final static String LEFT_SIDE = "L";
    private final static String RIGHT_SIDE = "R";
    private final static char DECIPHER_MODE = 'D';
    private final static char CIPHER_MODE = 'C';

    public static LinkedList<Integer> algorithm(ArrayList<Integer> message, ArrayList<Integer> key,char mode) {
        int row, col;
        LinkedList<Integer>out ;
        LinkedList<Integer> permutedMessage = permute(message, DesConstances.IP);
        ArrayList<LinkedList<Integer>> subKeys = createSubKeys(key);
        HashMap<String, LinkedList<Integer>> splittedMessage = splitList(permutedMessage);
        LinkedList<Integer> leftSideOfMessage = splittedMessage.get(LEFT_SIDE);
        LinkedList<Integer> rightSideOfMessage = splittedMessage.get(RIGHT_SIDE);
        LinkedList<Integer> rightSideAfterOperations;
        LinkedList<Integer> copyOfLeft;
        LinkedList<Integer> subKey=null;

        for (int i = 0; i < 16; i++) {
            if(mode==CIPHER_MODE){
                subKey=subKeys.get(i);
            }else if(mode==DECIPHER_MODE){
                subKey=subKeys.get(subKeys.size()-1-i);
            }

            rightSideAfterOperations = calculateRightSide(rightSideOfMessage, subKey);

            copyOfLeft= new LinkedList<>(leftSideOfMessage);

            leftSideOfMessage=rightSideOfMessage;
            rightSideOfMessage=rightSideAfterOperations;
            System.out.println("Right: "+rightSideOfMessage);
            rightSideOfMessage=xor(copyOfLeft,rightSideAfterOperations);
            System.out.println("After xor: "+rightSideOfMessage);
            System.out.println("Left: "+copyOfLeft);

        }

        out=mergeLeftAndRight(rightSideOfMessage,leftSideOfMessage);
        out=permute(out,DesConstances.REVERSE_IP);

        return out;
    }

    private static LinkedList<Integer> calculateRightSide(LinkedList<Integer> rightSideOfMessage, LinkedList<Integer> subKey) {
        int indexOfSubList = 0;
        LinkedList<Integer> rightSideAfterSValuesCalculated = new LinkedList<>();
        LinkedList<Integer> rightSide;
        rightSide = permute(rightSideOfMessage, DesConstances.E);
        LinkedList<Integer> temp;
        rightSide= xor(rightSide, subKey);
   /*     System.out.println("After XOR");
        rightSide.forEach(System.out::print);
        System.out.println();
*/
        for (int i = 0; i < 8; i++) {

            temp = calculateOfSBlock(rightSide.subList(indexOfSubList, indexOfSubList + 6), LIST_OF_S_BLOCKS.get(i));



            rightSideAfterSValuesCalculated.addAll(temp);

            indexOfSubList+=6;
        }
       // System.out.println();
       // System.out.println("Poloczony: "+ rightSideAfterSValuesCalculated);
       // System.out.println();


        rightSideAfterSValuesCalculated = permute(rightSideAfterSValuesCalculated, DesConstances.P);
       // System.out.println("Po permutacji: "+ rightSideAfterSValuesCalculated);

       // System.out.println();
        return rightSideAfterSValuesCalculated;


    }

    public static LinkedList<Integer> calculateOfSBlock(List<Integer> partOfMessage, int[][] sBlock) {
        int row = (partOfMessage.get(0) << 1) | partOfMessage.get(partOfMessage.size() - 1);
        int col = partOfMessage.get(1);

        for (int i = 2; i <= partOfMessage.size() - 2; i++) {
            col = col << 1;
            col = col | partOfMessage.get(i);

        }
       // System.out.println("Row "+row+ " Col: "+col);
        String binaryStringValuOfSTable = Integer.toBinaryString(sBlock[row][col]);
        if(sBlock[row][col]<=1)binaryStringValuOfSTable="000"+binaryStringValuOfSTable;
        if(sBlock[row][col]>1&sBlock[row][col]<=3)binaryStringValuOfSTable="00"+binaryStringValuOfSTable;
        if(sBlock[row][col]>3&sBlock[row][col]<=7)binaryStringValuOfSTable="0"+binaryStringValuOfSTable;


        LinkedList<Integer> binaryOutValue = new LinkedList<>();
        for (int i = 0; i < binaryStringValuOfSTable.length(); i++) {
            binaryOutValue.add(Character.getNumericValue(binaryStringValuOfSTable.charAt(i)));
        }
       // System.out.println("Blosck S: "+ binaryOutValue);

        return binaryOutValue;
    }

    public static ArrayList<LinkedList<Integer>> createSubKeys(ArrayList<Integer> key) {
       // System.out.println("Permuted Key: ");
        LinkedList<Integer> permutedKey = permute(key, DesConstances.PC_1);
       // permutedKey.forEach(System.out::print);
       // System.out.println();
       // System.out.println();
        HashMap<String, LinkedList<Integer>> splittedKey = splitList(permutedKey);

        LinkedList<Integer> leftSideOfKey = splittedKey.get(LEFT_SIDE);
       // System.out.println("Left side:");
      //  leftSideOfKey.forEach(System.out::print);
       // System.out.println();
       // System.out.println();
        LinkedList<Integer> rightSideOfKey = splittedKey.get(RIGHT_SIDE);
       // System.out.println("Right side:");
       // rightSideOfKey.forEach(System.out::print);
       // System.out.println();
       // System.out.println();
        LinkedList<Integer> mergedKey;
        ArrayList<LinkedList<Integer>> subKeyes = new ArrayList<>();
       // System.out.println("Keys after shifts: ");
        for (int i = 0; i < 16; i++) {
            if (i == 0 || i == 1 || i == 8 || i == 15) {

                leftSideOfKey=makeShiftsBitsOnOneSide(leftSideOfKey, 1);
              /*  System.out.println(i+1+". Left");
                leftSideOfKey.forEach(System.out::print);
                System.out.println();
                System.out.println();*/
                rightSideOfKey=makeShiftsBitsOnOneSide(rightSideOfKey, 1);
             /*   System.out.println(i+1+" Right:");
                rightSideOfKey.forEach(System.out::print);
                System.out.println();
                System.out.println();*/


            } else {
                leftSideOfKey=makeShiftsBitsOnOneSide(leftSideOfKey, 2);
               /* System.out.println(i+1+". Left");
                leftSideOfKey.forEach(System.out::print);
                System.out.println();
                System.out.println();*/

               rightSideOfKey= makeShiftsBitsOnOneSide(rightSideOfKey, 2);
                /*System.out.println(i+1+" Right:");
                rightSideOfKey.forEach(System.out::print);
                System.out.println();
                System.out.println();*/
            }
            mergedKey = mergeLeftAndRight(leftSideOfKey, rightSideOfKey);
           /* System.out.println("After megrge ");
            mergedKey.forEach(System.out::print);
            System.out.println();*/

            subKeyes.add(permute(mergedKey, DesConstances.PC_2));
         /*   System.out.println("Permuted Subkeys");
            permute(mergedKey, DesConstances.PC_2).forEach(System.out::print);
            System.out.println();
            System.out.println();*/
        }
        return subKeyes;
    }

    public static LinkedList<Integer> permute(List<Integer> toPermute, ArrayList<Integer> permutation) {
        LinkedList<Integer> permutedList = new LinkedList<>();
        for (Integer lookUpIndex : permutation) {
            permutedList.add(toPermute.get(lookUpIndex - 1));
        }
        return permutedList;
    }

    public static HashMap<String, LinkedList<Integer>> splitList(List<Integer> orginalList) {
        HashMap<String, LinkedList<Integer>> splittedList = new HashMap<>();
        splittedList.put("L", new LinkedList<>(orginalList.subList(0, orginalList.size() / 2)));
        splittedList.put("R", new LinkedList<>(orginalList.subList(orginalList.size() / 2, orginalList.size())));
        return splittedList;
    }

    public static LinkedList<Integer>makeShiftsBitsOnOneSide(LinkedList<Integer> oneSplittedSide, int numberOfShifts) {
        for (int i = 0; i < numberOfShifts; i++) {
            int temp = oneSplittedSide.get(0);
           oneSplittedSide.removeFirst();
           oneSplittedSide.add(temp);
        }

         return oneSplittedSide;

    }

    private static LinkedList<Integer> mergeLeftAndRight(LinkedList<Integer> left, LinkedList<Integer> right) {
        LinkedList<Integer> mergedList = new LinkedList<>();
        mergedList.addAll(left);
        mergedList.addAll(right);
        return mergedList;
    }

    private static LinkedList<Integer> xor(LinkedList<Integer> message, LinkedList<Integer> subKey) {
        LinkedList<Integer> xoredMessageWithSubKey = new LinkedList<>();
        for (int i = 0; i < message.size(); i++) {
            xoredMessageWithSubKey.add(message.get(i) ^ subKey.get(i));
        }
        return xoredMessageWithSubKey;
    }

}
