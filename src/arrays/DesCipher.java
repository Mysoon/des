package arrays;

import java.util.ArrayList;
import java.util.LinkedList;

public class DesCipher {
    public static LinkedList<Integer> cipher(ArrayList<Integer> message, ArrayList<Integer> key){

        return  SharedOperations.algorithm( message,  key,'C');
    }
}
